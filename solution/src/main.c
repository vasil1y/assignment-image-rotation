#include "../include/io.h"
#include "../include/transforms.h"
#include <stdio.h>
#include <stdlib.h>

#define FILE_OPEN_MODE "rb"

int main(int argc, char* argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect command using\n");
        fprintf(stderr, "Example: %s <code-image-file> <rotated-image-file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* source_image_filename = argv[1];
    char* rotated_image_filename = argv[2];

    FILE* input_image_file = fopen(source_image_filename, FILE_OPEN_MODE);
    if (!input_image_file) {
        perror("Source image opening error");
        return EXIT_FAILURE;
    }

    struct image* image = NULL;
    enum io_result_code read_result_code = from_bmp_file(input_image_file, &image);
    fclose(input_image_file);

    if (read_result_code != SUCCESS) {
        fprintf(stderr, "Image reading error: %d\n", read_result_code);
        if (image)
            image_delete(image);
        return EXIT_FAILURE;
    }

    struct image* rotated_image = rotate(image);
    if (!rotated_image) {
        fprintf(stderr, "Image rotating error\n");
        image_delete(image);
        return EXIT_FAILURE;
    }

    FILE* output_image_file = fopen(rotated_image_filename, "wb");
    if (!output_image_file) {
        perror("Rotated image file creating error");
        image_delete(image);
        image_delete(rotated_image);
        return EXIT_FAILURE;
    }

    enum io_result_code write_result_code = to_bmp_file(output_image_file, rotated_image);
    fclose(output_image_file);

    if (write_result_code != SUCCESS) {
        fprintf(stderr, "Rotated image writing error: %d\n", write_result_code);
        image_delete(image);
        image_delete(rotated_image);
        return EXIT_FAILURE;
    }

    image_delete(image);
    image_delete(rotated_image);
    printf("Rotated image was saved to %s \n", rotated_image_filename);
    return EXIT_SUCCESS;
}
