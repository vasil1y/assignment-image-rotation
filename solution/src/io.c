#include "../include/io.h"

#define BF_TYPE 0x4D42
#define BF_DATA_OFFSET 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_SIZE_IMAGE 0
#define BI_PELS_PER_METER 0
#define BI_CRL_USED 0
#define BI_CRL_IMPORTANT 0


static uint32_t get_padding(uint32_t row_length) {
    return (4 - (row_length % 4)) % 4;
}

static struct bmp_header create_header(const struct image* image) {
    uint32_t row_length = image->width * sizeof(struct pixel);
    return (struct bmp_header) {
        .bfType = BF_TYPE,
        .bfileSize = BF_DATA_OFFSET + (row_length + get_padding(row_length)) * image->height,
        .bfReserved = 0,
        .bOffBits = BF_DATA_OFFSET,
        .biSize = BI_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = BI_PLANES,
        .biBitCount = BI_BIT_COUNT,
        .biCompression = BI_COMPRESSION,
        .biSizeImage = BI_SIZE_IMAGE,
        .biXPelsPerMeter = BI_PELS_PER_METER,
        .biYPelsPerMeter = BI_PELS_PER_METER,
        .biClrUsed = BI_CRL_USED,
        .biClrImportant = BI_CRL_IMPORTANT
    };
}


enum io_result_code to_bmp_file(FILE* out, const struct image* image) {
    struct bmp_header header = create_header(image);
    uint32_t row_length = image->width * sizeof(struct pixel);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return HEADER_WRITE_ERROR;

    for (uint32_t y = 0; y < image->height; ++y) {
        if (fwrite(&image->data[y * image->width], sizeof(struct pixel), image->width, out) != image->width)
            return PIXELS_WRITE_ERROR;
        for (uint32_t p = 0; p < get_padding(row_length); ++p)
            if (fputc(0, out) == EOF)
                return PADDING_WRITE_ERROR;
    }
    return SUCCESS;
}

enum io_result_code from_bmp_file(FILE* in, struct image** image) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return BAD_HEADER;

    if (header.bfType != BF_TYPE)
        return BAD_SIGNATURE;

    *image = image_create(header.biWidth, header.biHeight);
    if (!(*image))
        return MEMORY_ALLOCATION_ERROR;

    uint32_t row_length = (*image)->width * sizeof(struct pixel);
    uint32_t padding = get_padding(row_length);
    for (uint32_t y = 0; y < header.biHeight; ++y) {
        if (fread(&(*image)->data[y * header.biWidth], sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
            if (feof(in) != 0) {
                return READ_UNEXPECTED_EOF;
            }
            return READ_INVALID_DATA;
        }
        fseek(in, (long) padding, SEEK_CUR);
    }
    return SUCCESS;
}
