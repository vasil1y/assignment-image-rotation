#include "../include/transforms.h"
#include <stddef.h>

struct image* rotate(const struct image* image) {
    struct image* rotated_image = image_create(image->height, image->width);
    if (!rotated_image)
        return NULL;
    for (uint64_t y = 0; y < image->height; ++y)
        for (uint64_t x = 0; x < image->width; ++x)
            rotated_image->data[x * rotated_image->width + rotated_image->width - 1 - y] = image->data[y * image->width + x];
    return rotated_image;
}
