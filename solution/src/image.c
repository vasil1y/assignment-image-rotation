#include "../include/image.h"
#include <stdlib.h>

struct image* image_create(const uint64_t width, const uint64_t height) {
    struct image* image = (struct image*) malloc(sizeof(struct image));
    image->width = width;
    image->height = height;
    image->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));

    if (!image->data) {
        free(image);
        return NULL;
    } return image;
}

void image_delete(struct image* image) {
    if (image) {
        free(image->data);
        free(image);
    }
}
