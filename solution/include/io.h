#ifndef IO_H
#define IO_H

#include "image.h"
#include <stdio.h>

enum io_result_code {
    SUCCESS = 0,
    BAD_SIGNATURE,
    MEMORY_ALLOCATION_ERROR,
    BAD_HEADER,
    HEADER_WRITE_ERROR,
    PIXELS_WRITE_ERROR,
    PADDING_WRITE_ERROR,
    READ_UNEXPECTED_EOF,
    READ_INVALID_DATA
};

enum io_result_code from_bmp_file(FILE* in, struct image** image);
enum io_result_code to_bmp_file(FILE* out, const struct image* image);

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    __attribute__((unused)) uint32_t bfileSize;
    __attribute__((unused)) uint32_t bfReserved;
    __attribute__((unused)) uint32_t bOffBits;
    __attribute__((unused)) uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    __attribute__((unused)) uint16_t biPlanes;
    __attribute__((unused)) uint16_t biBitCount;
    __attribute__((unused)) uint32_t biCompression;
    __attribute__((unused)) uint32_t biSizeImage;
    __attribute__((unused)) uint32_t biXPelsPerMeter;
    __attribute__((unused)) uint32_t biYPelsPerMeter;
    __attribute__((unused)) uint32_t biClrUsed;
    __attribute__((unused)) uint32_t biClrImportant;
};

#endif
