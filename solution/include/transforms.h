#ifndef TRANSFORMS_H
#define TRANSFORMS_H

#include "image.h"

struct image* rotate(const struct image* image);

#endif
